package com.sis.sd.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmaGatewayConfig {
	
	@Bean
	public RouteLocator routeLocator(RouteLocatorBuilder builder) {
		
		return builder.routes()
				.route("companyServiceId", r->r.path("/company/**").uri("lb://COMPANY-SERVICE"))
				.route("customerServiceId", r->r.path("/customer/**").uri("lb://CUSTOMER-SERVICE"))
				.route("mediatorServiceId", r->r.path("/mediator/**").uri("lb://MEDIATOR-SERVICE"))
				.route("stockServiceId", r->r.path("/stock/**").uri("lb://STOCK-SERVICE"))
				.build();
	}

}
